public class TAD_Heap implements TAD_IHeap{
	private int[] valores;
	private int pos;
	
	public void cria_heap(int n) {
		valores = new int[n+1];
		pos = 0;
	}

	public void insere_heap(int val){
		if (pos < valores.length-1 ){
			valores[pos] = val;
			corrige_acima(pos);
			pos++;
		}
		else
			System.out.println("Heap CHEIA!\n");
	}

	public void imprime_heap() {
		for(int i =0; i < pos; i++)
			System.out.println("Indice = " + i + " valor = " + valores[i]);
		System.out.println();
	}

	public int busca_heap(int val) {
		for(int i =0; i < pos; i++)
			if(valores[i] == val)
				return i;
		return -1;
	}

	public int remove_heap() {
		if (pos > 0){
			int topo = valores[0];
			valores[0] = valores[pos-1];
			pos--;
			corrige_abaixo();
			return topo;
		}
		
		else {
			System.out.println("Heap vazia");
			return -1;
		}
	}

	public void libera_heap() {
		valores = null;
	}

	@Override
	public void edita_heap(int val, int novo) {
		int pai = busca_heap(val);
		if(pai != -1 && novo > val){
			valores[pai] = novo;
			corrige_acima(pai);
		}
		else{
			valores[pai] = novo;
			corrige_abaixo();
		}
	}
	
	private void corrige_acima(int pos) {
		while (pos > 0){
			int pai = (pos-1)/2;
			if (valores[pai] < valores[pos])
				troca(pos, pai);
			else
				break;
			pos = pai;
		}
	}
	
	private void troca(int a, int b) {
		int f = valores[a];
		valores[a] = valores[b];
		valores[b] = f;
	}
	
	private void corrige_abaixo(){
		int pai=0;

		while (2*pai+1 < pos){
			int filho_esq = 2*pai+1;
			int filho_dir = 2*pai+2;
			int filho;
			
			if (filho_dir >= pos) 
				filho_dir = filho_esq;
			
			if (valores[filho_esq] > valores[filho_dir])
				filho = filho_esq;
			else
				filho = filho_dir;
			
			if (valores[pai] < valores[filho])
				troca(pai, filho);
			else
				break;
			pai = filho;
		}
	}
	
	public int getValorByID(int i){
		return valores[i];
	}
}

public interface TAD_IHeap {
	void cria_heap(int n);
	void insere_heap(int val);
	void imprime_heap();
	int busca_heap(int val);
	int remove_heap();
	void edita_heap(int val, int novo);
	void libera_heap();
}

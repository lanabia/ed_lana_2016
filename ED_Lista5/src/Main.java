import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		System.out.println("Tamanho Heap");
		Scanner sc = new Scanner(System.in);
		int tamanho = sc.nextInt();
		sc.close();
		
		//1. Criar a estrutura de dados (Construção da Heap);
		TAD_Heap heap = new TAD_Heap();	
		heap.cria_heap(tamanho);
		
		//2. Inserir um elemento;
		for(int i =0; i < tamanho; i++)
			heap.insere_heap(i);
		
		heap.imprime_heap();
		
		//3. Recuperar/Buscar um determinado elemento;
		int nova = 0;
		nova = heap.busca_heap(5);
		System.out.println(heap.getValorByID(nova));
		System.out.println();
	
		//4. Remover um determinado elemento;
		heap.remove_heap();
		heap.remove_heap();
		heap.imprime_heap();
		
		//5. Alterar o valor de um determinado elemento;
		heap.edita_heap(4, 27);
		heap.imprime_heap();
		
		//6. Liberar a estrutura de dados;
		heap.libera_heap();
		
	}
}

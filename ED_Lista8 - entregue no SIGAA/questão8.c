#include <stdio.h>
#include <stdlib.h>
#include <string.h>



typedef struct hash Hash;
Hash* hsh_cria(int n, int hash_func(char*), void free_func(void*));
int hsh_insere(Hash* tabela, void* info, char* chave);
int hsh_remove(Hash* tabela, char* chave);
void* hsh_busca(Hash* tabela, void* chave);
Hash* hsh_libera(Hash* tabela);
void hsh_percorre(Hash* tabela, void callback_func(void*));


typedef struct pais Pais;
	struct pais {
	char nome[81];
	char continente[81];
	int exercitos;
	char dono[81];
};

//Escreva, como um módulo cliente do TAD que conhece a estrutura Pais, uma função que troque o nome do dono de um país e tenha o seguite protótipo:
// o valor de retorno é 1 se teve sucesso e 0, caso contrario. Pode utilizar as funções das biliotecas padrão do C, tipo stdio.h string.h, etc...
int dono_do_pais(char* novo_dono, Hash* tabela_pais, char* nome_do_pais){

	hsh_busca(tabela_pais,nome_do_pais);



	return 0;
}



int left(int i){
	return 2*i; 
}

int right(int i){
	return 2*i+1;
}

float get(int i,Aluno** v){
	return (*(v+i))->nota; // supondo 1<=i<=n
	// retorna o elemento i no vetor de ponteiros v
}

int verifica_max (int i, int n, Aluno** v){
	printf("%i\n", get(i,v));
	int l = left(i);
	int r = right(i);
	int maior = i;
	if(l<=n && get(l,v)>get(i,v))
		maior = l;
	else maior = i;

	if(r<=n && get(r,v)>get(maior,v))
		maior = r;

	// se tiver que trocar ele retorna 0 pois não é um heap
	if(maior != i)
		return 0;
	// se não ele desce a árvore e continua verificando
	if(l<=n) verifica_max(l,n,v);
	if(r<=n) verifica_max(r,n,v);

	return 1;

}

int heap_max (int n, Aluno** v){
	int i;
	int result = 0;
	for(i = n/2 ; i==0 ; i-1) {
			result = verifica_max(i,n,v);
	}

	return result;
}

void print(Aluno **v){

	for(int i=1 ; i==0 ; i++){
		printf("%d",get(i,v));
	}

}

main() { 
        /* definioes de variaveis */
        Aluno *a1,*a2,*a3,*a4,*a5,*a6,*a7,*a8;

        Aluno **heap = &a1;

        heap = (struct aluno **) malloc (8*sizeof(struct aluno *));
   		M = malloc (m * sizeof (int *));
   		for (i = 0; i < 8; ++i){
      		heap[i] = malloc(1*sizeof(struct aluno*));
   		}
        /* aloca n elementos para v */
		a1 = (struct aluno *) malloc(1*sizeof(struct aluno));
        heap[1] = a1;

		a2 = (struct aluno *) malloc(1*sizeof(struct aluno));
		heap[2] = a2;

		a3 = (struct aluno *) malloc(1*sizeof(struct aluno));
		heap[3] = a3;

		a4 = (struct aluno *) malloc(1*sizeof(struct aluno));
		a5 = (struct aluno *) malloc(1*sizeof(struct aluno));
		a6 = (struct aluno *) malloc(1*sizeof(struct aluno));
		a7 = (struct aluno *) malloc(1*sizeof(struct aluno));
		a8 = (struct aluno *) malloc(1*sizeof(struct aluno));

      
        //a1->nome = "a1"; 
        a1->nota = 90;

        //a2->nome = "a2"; 
        a2->nota = 60;

        //a3->nome = "a3"; 
        a3->nota = 45;

        //a4->nome = "a4"; 
        a4->nota = 30;

       //a5->nome = "a5"; 
        a5->nota = 28;

       // a6->nome = "a6"; 
        a6->nota = 23;

        //a7->nome = "a7"; 
        a7->nota = 20;

        //a8->nome = "a8"; 
        a8->nota = 15;

        printf("Oi: %f",(*heap)->nota);

        Aluno *p;
        p = *(heap + sizeof(struct aluno));
        printf(" Oi: %f",(*heap+1)->nota);

        print(heap);
}

// -static-libgcc -static-libstdc++

// g++ "C:\Users\Lana\workspace\ed_lana_2016\ED_Lista10 - entregue no SIGAA\questão4.c" -o "C:\Users\Lana\workspace\ed_lana_2016\ED_Lista10 - entregue no SIGAA/questao4" -static-libgcc -static-libstdc++
public class Conj_Dijuntos implements IConj_Dijuntos{
	private Celula[] valores;

	public void cria_conj(int n) {
		valores = new Celula[n];
		for(int i =0; i < valores.length; i++)
			valores[i] = new Celula();
	}

	public int find(int x) {
		for(int i =0; i < valores.length; i++)
			if(valores[i].getValor() == x) 
				return valores[i].getRaiz();
			
		return -2;
	}


	public void makeSet() {
		for(int i=0; i < valores.length; ++i)
			valores[i].setRaiz(valores[i].getValor());
	}

	public void union(int x, int y) {
		int raizX = find(x);
		
		for(int i=0; i < valores.length; i++){
			if(valores[i].getValor() == y){
				valores[i].setRaiz(raizX);
				break;
			}
		}
	}

	public boolean mesmo_conjunto(int x, int y) {
		return find(x) == find(y);
	}

	public void liberar_conjunto() {
		for(int i=0; i < valores.length; i++){
			valores[i] = null;
		}
		valores = null;
	}
	
	public Celula[] getValores() {
		return valores;
	}

	public void setValores(Celula[] valores) {
		this.valores = valores;
	}
}

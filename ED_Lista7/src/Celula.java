public class Celula{
	
	private int valor;
	private int raiz;

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public int getRaiz() {
		return raiz;
	}

	public void setRaiz(int raiz) {
		this.raiz = raiz;
	}
	
}

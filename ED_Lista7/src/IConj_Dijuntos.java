public interface IConj_Dijuntos {
	void cria_conj(int n);
	int find(int x);
	void makeSet();
	void union(int x, int y);
	boolean mesmo_conjunto(int x, int y);
	void liberar_conjunto();
}

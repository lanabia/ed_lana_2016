import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		System.out.println("Tamanho Conjunto");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		sc.close();
		
		String valores[] = new String[n];	
		for(int i =0; i < n; i++){
			valores[i] = ""+i;
		}
		
		
		//1. Criar a estrutura de dados (ou seja, criar um determinado conjunto);
		Conjunto conjunto = new Conjunto();	
		conjunto.cria_conjunto(n, valores);
		
		//2. Inserir um elemento em um determinado conjunto;
		conjunto.insere_conjunto("0");
		conjunto.insere_conjunto("1");
		conjunto.insere_conjunto("2");
		conjunto.insere_conjunto("3");
		conjunto.imprimir_conjunto();
		
		//3. Remover um elemento de um determinado conjunto;
		conjunto.remove_conjunto("2");
		conjunto.imprimir_conjunto();
		
		//4. Fazer a uni�o entre dois conjuntos;
		Conjunto conjunto2 = new Conjunto();	
		conjunto2.cria_conjunto(n, valores);
		conjunto2.insere_conjunto("3");
		conjunto2.insere_conjunto("5");
		
		conjunto2.setBits(conjunto.uniao_conjuntos(conjunto2));
		
		conjunto2.imprimir_conjunto();
		
		//5. Fazer a interse��o entre dois conjuntos;
		Conjunto conjunto3 = new Conjunto();	
		conjunto3.cria_conjunto(n, valores);
		conjunto3.insere_conjunto("2");
		conjunto3.insere_conjunto("1");
		
		conjunto3.setBits(conjunto.interse��o_conjuntos(conjunto3));
		conjunto3.imprimir_conjunto();
		
		//6. Fazer a diferen�a entre dois conjuntos;
		conjunto2.setBits(conjunto2.diferen�a_conjuntos(conjunto3));
		conjunto2.imprimir_conjunto();
		
		//7. Verificar se um conjunto A � subconjunto de B;
		conjunto.imprimir_conjunto();
		
		if(conjunto.verifica_subconjunto(conjunto2))
			System.out.println("Conjunto 2 � subconjunto de conjunto 1");
		else
			System.out.println("Conjunto 2 n�o � subconjunto de conjunto 1");
		
		//8. Verificar se dois conjuntos s�o iguais;
		if (conjunto.verifica_conjuntos_iguais(conjunto2))
			System.out.println("Conjuntos iguais\n");
		else
			System.out.println("Conjuntos diferentes\n");
		
		//9. Gerar o complemento de um determinado conjunto;
		conjunto3.setBits(conjunto.complemento_conjunto());
		conjunto3.imprimir_conjunto();
		
		//10. Verificar se um elemento pertence a um determinado conjunto;
		if(conjunto.pertence_conjunto("1"))
			System.out.println("Pertence \n");
		else
			System.out.println("N�o Pertence \n");
		
		//11. Recuperar o n�mero de elementos de um determinado conjunto;
		System.out.println(conjunto.numero_elementos_conjunto());
		
		//12. Liberar a estrutura de dados (um determinado conjunto);
		conjunto.libera_conjunto();
		conjunto2.libera_conjunto();
	}
}

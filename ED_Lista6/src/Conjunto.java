public class Conjunto implements iConjunto{
	private String[] valores;
	private int bits;

	public void cria_conjunto(int n, String[] valores) {
		this.valores = valores;
		bits = 0;
	}

	public void insere_conjunto(String val) {
		for(int i =0; i < valores.length; i++){
			if(valores[i].equals(val)){
				ligarBit(i);
				break;
			}
		}
	}

	public void remove_conjunto(String val) {
		for(int i =0; i < valores.length; i++){
			if(valores[i].equals(val)){
				desligarBit(i);
				break;
			}
		}
	}

	public int uniao_conjuntos(Conjunto conj) {
		return conj.getBits() | bits;
	}

	public int interse��o_conjuntos(Conjunto conj) {
		return conj.getBits() & bits;
	}

	public int diferen�a_conjuntos(Conjunto conj) {
		if(conj.getBits() < bits)
			return bits - conj.getBits();
		else
			return conj.getBits() - bits;
	}

	public boolean verifica_subconjunto(Conjunto conj) {
		if((bits & conj.getBits()) == conj.getBits() && (bits | conj.getBits()) == bits)
			return true;
		return false;
	}

	public boolean verifica_conjuntos_iguais(Conjunto conj) {
		return (bits == conj.getBits());
	}

	public int complemento_conjunto() {
		return ~bits;
		
	}

	public boolean pertence_conjunto(String val) {
		for(int i =0; i < valores.length; i++)
			if(valores[i].equals(val))
				return estaLigado(i);
		
		return false;
	}

	public int numero_elementos_conjunto() {
		int valor = bits;
		int bit;
		int contador =0;
		while(valor != 0){
			bit = valor%2;
			if(bit == 1)
				contador ++;
			valor = valor/2;
		}
		return contador;
	}

	public void libera_conjunto() {
		for(int i =0; i < valores.length; i++){
			valores[i] = null;
		}
		valores = null;
	}
	
	public void ligarBit(int i) {
		if(!estaLigado(i))
			bits += (int) Math.pow(2, i);
	}
	public void desligarBit(int i) {
		if(estaLigado(i))
			bits -= (int) Math.pow(2, i);
	}
	
	public String[] getValores() {
		return valores;
	}

	public void setValores(String[] valores) {
		this.valores = valores;
	}

	public int getBits() {
		return bits;
	}

	public void setBits(int bits) {
		this.bits = bits;
	}
	
	public boolean estaLigado(int i){
		int mask =  1 << i;
		return ((mask & bits) != 0);
	}
	
	public void imprimir_conjunto() {
		for(int i =0; i < valores.length; i++){
			if(estaLigado(i))
				System.out.println(valores[i]);
			
		}
		System.out.println();
	}
}

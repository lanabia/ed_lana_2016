public interface iConjunto {
	void cria_conjunto(int n, String[] valores);
	void insere_conjunto(String val);
	void remove_conjunto(String val);
	int uniao_conjuntos(Conjunto conj);
	int interse��o_conjuntos(Conjunto conj);
	int diferen�a_conjuntos(Conjunto conj);
	boolean verifica_subconjunto(Conjunto conj);
	boolean verifica_conjuntos_iguais(Conjunto conj);
	int complemento_conjunto();
	boolean pertence_conjunto(String val);
	int numero_elementos_conjunto();
	void libera_conjunto();

}

public class Elemento{
	

	private int info;
	private Elemento proximo;

	public Elemento cria_lst() {	
		Elemento lst = new Elemento();
		return lst;
	}

	public Elemento insere_lst(Elemento lst, int val) {
		Elemento nova = new Elemento();
		this.info = val;
		nova.setProximo(lst); 
		
		return nova;
	}

	public void imprime_lst(Elemento lst) {
		Elemento prox = lst.getProximo();
		System.out.print("Hash ");
		while(prox != null){
			System.out.print(prox.getInfo() + " -> ");
			prox = prox.getProximo();
		}	
		System.out.print("null\n");
	}

	public void imprime_lst_rec(Elemento lst) {
		if(lst.getProximo() != null){
			System.out.println("rec = " + lst.getProximo().getInfo());
			imprime_lst_rec(lst.getProximo());
		}
	}

	public void imprime_lst_rev(Elemento lst) {
		if(lst.getProximo() != null){
			imprime_lst_rev(lst.getProximo());
			System.out.println("rev = " + lst.getProximo().getInfo());	
		}
	}

	public boolean lst_vazia(Elemento lst) {
		return (lst.getProximo() == null);
	}

	public Elemento busca_lst(Elemento lst, int val) {
		Elemento prox = lst;
		while(prox != null){
			if (prox.getInfo() == val)
				return prox;
			prox = prox.getProximo();
		}
		return null;
	}

	public void remove_lst(Elemento lst, int val) {
		Elemento ant = null;
		if(busca_lst(lst, val) != null){
			while (lst != null && lst.getInfo() != val) {
				ant = lst;
				lst = lst.getProximo();
			}
			
			if(ant == null){
				lst = lst.getProximo();
			}
			else {
				ant.setProximo(lst.getProximo());
				lst = ant;
			}
		}
	}

	public Elemento remove_lst_rec(Elemento lst, int val) {
		if (!lst_vazia(lst)) {			
			if (lst.getInfo() == val) 
				lst = lst.getProximo();
			else 
				lst.setProximo(remove_lst_rec(lst.getProximo(), val));
		}
		return lst;
	}

	public Elemento libera_lst(Elemento lst) {
		Elemento libera;
		while(lst != null){
			libera = lst.getProximo();
			lst = null;
			lst = libera; 
		}	
		
		return lst;
	}

	public int getInfo() {
		return info;
	}

	public void setInfo(int info) {
		this.info = info;
	}

	public Elemento getProximo() {
		return proximo;
	}

	public void setProximo(Elemento proximo) {
		this.proximo = proximo;
	}
	
}

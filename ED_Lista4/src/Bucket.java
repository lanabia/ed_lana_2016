public class Bucket {
	private Integer[] valores;
	private int referencia;
	
	public Bucket(int tamanho) {
		this.valores = new Integer[tamanho];
		referencia = 0;
	}

	public boolean insere_bucket(int val) {
		for(int i =0; i < valores.length; i++){
			if (valores[i] == null){
				valores[i] = val;
				return true;
			}
		}
		return false;
	}
	
	public int getReferencia(){
		return referencia;
	}
	
	public void setReferencia(int referencia){
		this.referencia = referencia;
	}
	
	public Integer getValor(int indice){
		return valores[indice];
	}

	public Integer[] getValores(){
		return valores;
	}
	
	public void setValor(int valor){
		for(int i =0; i < valores.length; i++){
			if(valores[i] == null){
				valores[i] = valor;
				break;
			}
		}
	}
	
	public void imprime_bucket() {
		for(int i =0; i < valores.length; i++){
			if(valores[i] != null)
				System.out.print(valores[i] + " ");
		}
		
	}

	public Integer busca_bucket(int val) {
		for(int i =0; i < valores.length; i++){
			if(valores[i] == val)
				return val;
		}
		return null;
	}

	public void remove_bucket(Integer val) {
		for(int i =0; i < valores.length - 1; i++){
			if(valores[i] == val){
				valores[i] = null;
				break;
			}
		}
	}

	public void libera_bucket() {
		for(int i =0; i < valores.length; i++){
			valores[i] = null;
		}
	}
}

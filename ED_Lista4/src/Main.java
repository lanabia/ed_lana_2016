import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		System.out.println("N chaves");
		Scanner sc = new Scanner(System.in);
		int n_chaves = sc.nextInt();
		System.out.println("Tamanho bucket");
		int tamanho = sc.nextInt();
		sc.close();
		
		//1. Criar a estrutura de dados (hash estendível);
		TAD_Hash hash = new TAD_Hash();	
		hash.cria_hash(n_chaves, tamanho);
		
		//2. Inserir elemento;
		for(int i =1; i <= n_chaves; i++)
			hash.insere_hash(i);
		
		hash.imprime_hash();
		
		//3. Recuperar/Buscar um determinado elemento;
		int nova = 0;
		nova = hash.busca_hash(5);
		System.out.println(nova);
	
		//4. Remover um determinado elemento;
		hash.remove_hash(6);
		hash.remove_hash(7);
		hash.remove_hash(10);
		hash.imprime_hash();
		
		//5. Liberar a estrutura de dados (hash estendível);
		hash.libera_hash();
	}
}

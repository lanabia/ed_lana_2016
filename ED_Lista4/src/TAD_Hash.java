public class TAD_Hash implements TAD_IHash{
	private Bucket[] bucket;
	private int bits_significativos;
	private int tamanho_bucket;
	
	public void cria_hash(int n, int tamanho) {
		this.tamanho_bucket = tamanho;
		bits_significativos = 1;
		bucket = new Bucket[1];
		bucket[0] = new Bucket(tamanho_bucket);
	}

	public void insere_hash(int val){
		int indice = calcula_indice(val);
		if(indice%2 == 0){	
			if(bucket[indice].getReferencia() <  bits_significativos){
				if(!bucket[indice].insere_bucket(val)){
					divide_bucket(indice);
					insere_hash(val);
				}
			}
			else{
				if(!bucket[indice].insere_bucket(val)){
					divide_tabela();
					insere_hash(val);
				}
			}
		}
		else{
			if(bucket[indice-1].getReferencia() <  bits_significativos){
				if(!bucket[indice-1].insere_bucket(val)){
					divide_bucket(indice-1);
					insere_hash(val);
						
				}
			}
			else if(!bucket[indice].insere_bucket(val)){
				divide_tabela();
				insere_hash(val);
			}
		}
	}				
	
	private void divide_bucket(int indice) {
		Bucket novo_bucket[] = new Bucket[bucket.length+1];
		
		for(int i=0; i < bucket.length; i++)
			novo_bucket[i] = bucket[i];
		
		novo_bucket[indice+1] = new Bucket(tamanho_bucket);
		novo_bucket[indice].setReferencia(bits_significativos);
		novo_bucket[indice+1].setReferencia(bits_significativos);
		
		for(int i=0; i < tamanho_bucket; i++){
			int valor = bucket[indice].getValor(i);
			if(calcula_indice(valor) != indice){
				int novo_indice = calcula_indice(valor);
				novo_bucket[novo_indice].setValor(valor);
				novo_bucket[indice].remove_bucket(valor);
			}
		}

		bucket = novo_bucket;
	}

	public void imprime_hash() {
		for(int i =0; i < bucket.length; i++){
			System.out.print("Bucket " + i + " = ");
			bucket[i].imprime_bucket();
			System.out.println();
		}
	}

	public Integer busca_hash(int val) {
		int indice = calcula_indice(val);
		return bucket[indice].busca_bucket(val);
	}

	public void remove_hash(int val) {
		int indice = calcula_indice(val);
		bucket[indice].remove_bucket(val);
	}

	public void libera_hash() {
		for(int i =0; i < bucket.length; i++){
			bucket[i].libera_bucket();
			bucket[i] = null;
		}
	}
	
	private int calcula_indice(int val){
		int mask, masked_n = 0;
		
		for(int i=1; i <= bits_significativos; i++){
			mask =  1 << bits_significativos - i;
		    masked_n += val & mask;
		}
		return masked_n;
	}
	
	private void divide_tabela() {
		bits_significativos++;
		Bucket novo_bucket[] = new Bucket[(int) Math.pow(2,bits_significativos)];
		int tamanho_antigo = bucket.length;
		
		for(int i=0; i < tamanho_antigo; i++)
			novo_bucket[i] = bucket[i];
		
		
		bucket = new Bucket[novo_bucket.length];
		
		for(int i=0; i < bucket.length; i++){
			bucket[i] = new Bucket(tamanho_bucket);
			bucket[i].setReferencia(bits_significativos);
		}
		for(int i=0; i < tamanho_antigo; i++){
			for(int j=0; j < tamanho_bucket; j++){
				if(novo_bucket[i].getValor(j) != null)
					insere_hash(novo_bucket[i].getValor(j));
			}
		}
	}
}

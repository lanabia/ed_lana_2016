public interface TAD_IHash {
	void cria_hash(int n, int tamanho);
	void insere_hash(int val);
	void imprime_hash();
	Integer busca_hash(int val);
	void remove_hash(int val);
	void libera_hash();
}

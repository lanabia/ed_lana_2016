public interface TAD_IHash {
	void cria_hash(int n);
	void insere_hash(int val);
	void imprime_hash();
	Elemento busca_hash(int val);
	void remove_hash(int val);
	void libera_hash();
}

public class TAD_Hash implements TAD_IHash{
	private Elemento[] celulas;
	
	public void cria_hash(int n) {
		int tamanho = (n/2) + (n%2);
		celulas = new Elemento[tamanho];
		
		for(int i =0; i < tamanho; i++){
			celulas[i] = new Elemento();
			celulas[i].setProximo(null);
		}
	}

	public void insere_hash(int val){
		int indice = calcula_indice(val);
		celulas[indice] = celulas[indice].insere_lst(celulas[indice], val);
	}

	public void imprime_hash() {
		for(int i =0; i < celulas.length; i++)
			celulas[i].imprime_lst(celulas[i]);
	}

	public Elemento busca_hash(int val) {
		if(val < (celulas.length * 2) + 1){
			int indice = calcula_indice(val);
			return celulas[indice].busca_lst(celulas[indice], val);
		}
		return null;
	}

	public void remove_hash(int val) {
		int indice = calcula_indice(val);
		celulas[indice].remove_lst(celulas[indice], val);
	}

	public void libera_hash() {
		for(int i =0; i < celulas.length; i++){
			celulas[i].libera_lst(celulas[i]);
			celulas[i] = null;
		}
	}
	
	private int calcula_indice(int val){
		return val % celulas.length;
	}
}

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		System.out.println("N chaves");
		Scanner sc = new Scanner(System.in);
		int n_chaves = sc.nextInt();
		sc.close();
		
		//1. Criar a tabela de dispers�o;
		TAD_Hash hash = new TAD_Hash();	
		hash.cria_hash(n_chaves);
		
		//2. Inserir elemento;
		for(int i =1; i <= n_chaves; i++)
			hash.insere_hash(i);
		hash.imprime_hash();
		
		//3. Recuperar/Buscar um determinado;
		Elemento new_elemento = new Elemento();
		new_elemento = hash.busca_hash(5);
		System.out.println(new_elemento.getInfo());
	
		//4. Remover um determinado elemento;
		hash.remove_hash(6);
		hash.remove_hash(16);
		hash.remove_hash(16);
		hash.imprime_hash();
		
		//5. Liberar a a tabela de dispers�o;
		hash.libera_hash();
		
	}
}
